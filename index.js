/* 
    3. Cart class:

    contents property - array of objects with structure: {product: instance of Product class, quantity: number}

    totalAmount property - number

    addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property

    showCartContents() method - logs the contents property in the console

    updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.

    clearCartContents() method - empties the cart contents

    computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
*/

class Cart{
    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }

    addToCart(product, quantity){
        this.contents.push(new Order(product, quantity));
        return this;
    }

    showCartContents(){
        this.contents.length > 0? console.log('Cart contents: ', this.contents): console.log('This cart is empty');
        return this; 
    }

    updateProductQuantity(productName, newQuantity){
        if(this.contents.length > 0){
            this.contents.find(content => content.product.name === productName).quantity = newQuantity;
        } else {
            console.log('This cart is empty');
        }
        return this;
    }

    clearCartContents(){
        this.contents = [];
        return this;
    }

    computeTotal(){
        if(this.contents.length > 0 ){
            this.totalAmount = this.contents.reduce((subtotal, order) =>   subtotal + (order.quantity * order.product.price), 0)
        } else{
            console.log('This cart is empty')
        }

        return this;
    }
    
}

/* 
    1. Customer class:

    email property - string

    cart property - instance of Cart class

    orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}

    checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty

*/

class Customer{
    constructor(email){
        this.email = verifyValueType(email, "email", "string");
        this.cart = new Cart();
        this.orders = [];
    }

    checkout(){
        if(this.cart.contents.length > 0 ){
            this.orders.push(this.cart);
            this.cart = new Cart();
        }

        return this;
    }

}


/* 
    2. Product class:

    name property - string

    price property - number

    isActive property - Boolean: defaults to true

    archive() method - will set isActive to false if it is true to begin with

    updatePrice() method - replaces product price with passed in numerical value

*/

class Product{
    constructor(name, price){
        this.name = verifyValueType(name, "name", "string");
        this.price = verifyValueType(price, "price", "number");
        this.isActive = true;
    }

    archive(){
        this.isActive = false;
        return this;
    }

    updatePrice(newPrice){
        this.price = verifyValueType(newPrice, "price", "number");
        console.log(`The new price for ${this.name} is ${newPrice}`);
        return this;
    }
}

// Helper functions

// function to make sure that the value of a property is the correct one
// will return an error and not proceed with creation of object if not
function verifyValueType(value, property, expectedValue){
    if(typeof value === expectedValue){
        return value;
    } else {
        throw new TypeError(`Please make sure you are entering a ${expectedValue} for ${property} property`);
    }
}

// function to make sure that the object being used is of the correct type
// will return an error and not proceed with creation of object if not
function verifyObjectType(obj, property, expectedValue){
    if(obj instanceof expectedValue){
        return obj;
    } else {
        throw new TypeError(`Please make sure you are entering a ${expectedValue} object for ${property} property`);
    }
}

// used for creating an order product quantity pair
class Order{
    constructor(product, quantity){
        this.product = verifyObjectType(product, "product", Product);
        this.quantity = verifyValueType(quantity, "quantity", "number");
    }
}




